package main;
import java.util.*;
import static main.input.*;

public class kwic {

    public static String removeStopWord(String title, String[] stopwords){
        String[] words = title.split(" ");
        ArrayList<String> newTitle = new ArrayList<>();
        for (String word : words) {
            if (Arrays.asList(stopwords).contains(word)) {
                continue;
            }
            newTitle.add(word);
        }

        String[] stopWordsArr = new String[newTitle.size()];
        stopWordsArr = newTitle.toArray(stopWordsArr);

        StringJoiner joiner = new StringJoiner(" ");
        for (String value : stopWordsArr) {
            joiner.add(value);
        }


        return joiner.toString();
    }

    public static String keywordToUPPER(String title, String keyword) {
        String[] words = title.split(" ");
        boolean found = false;
        ArrayList<String> newTitle = new ArrayList<>();
        for (String word : words) {
            if (word.equals(keyword) && ! found) {
                newTitle.add(word.toUpperCase());
                found =true;
            }
            else {
                newTitle.add(word);
            }
        }

        String[] newTitleArr = new String[newTitle.size()];
        newTitleArr = newTitle.toArray(newTitleArr);

        StringJoiner joiner = new StringJoiner(" ");
        for (String value : newTitleArr) {
            joiner.add(value);
        }

        return joiner.toString();

    }

    public static HashMap<String, ArrayList<String>> createKWICMap(String[] titles, String[] stopwords){
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        for (String title : titles){
            String titleLower = title.toLowerCase();
            String nonStopWordTitle = removeStopWord(titleLower, stopwords);
            String[] newTitleArr = nonStopWordTitle.split(" ");

            for (String word : newTitleArr) {
                ArrayList<String> itemsList = map.get(word);
                String titleKeyword = keywordToUPPER(titleLower, word);

                // if list does not exist create it
                if(itemsList == null) {
                    itemsList = new ArrayList<String>();

                    itemsList.add(titleKeyword);
                    map.put(word, itemsList);
                } else {
                    if(!itemsList.contains(titleKeyword)) {
                        itemsList.add(titleKeyword);
                    }
                    else {
                        // need to disambiguate between multiple copies
                        String[] lowerWords = titleLower.split(" ");
                        boolean found = false;
                        for(int i=0; i<lowerWords.length; i++) {
                            if (lowerWords[i].equals(word) && found) {
                                lowerWords[i] = word.toUpperCase();
                            }
                            if (lowerWords[i].equals(word) && ! found) {
                                lowerWords[i] = word.toLowerCase();
                                found = true;
                            }
                        }
                        StringJoiner joiner = new StringJoiner(" ");
                        for (String value : lowerWords) {
                            joiner.add(value);
                        }
                        itemsList.add(joiner.toString());
                    }
                }
            }
        }
        return map;
    }

    public static void printMap(HashMap<String, ArrayList<String>> map) {
        // need to convert to TreeSet so I can sort it by key
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            ArrayList<String> values = map.get(key);
            for (String value : values) {
                StringJoiner joiner = new StringJoiner(" ");
                joiner.add(value);
                System.out.println(joiner.toString());
            }
        }
    }

    public static String returnMap(HashMap<String, ArrayList<String>> map) {
        String ans = "";
        // need to convert to TreeSet so I can sort it by key
        SortedSet<String> keys = new TreeSet<>(map.keySet());
        for (String key : keys) {
            ArrayList<String> values = map.get(key);

            for (String value : values) {
                StringJoiner joiner = new StringJoiner(" ");
                joiner.add(value);
                ans = ans + joiner.toString() + "\n";
            }
        }
        return ans;
    }

    public static void main(String[] args) {
        // Input format- type a stop word, followed by enter. Then enter "::" when done with stop words
        // Next input movie titles. when done, type "DONE" to stop the input process
        ArrayList<String> input = readInput();
        String[] stopWords = parseStopWords(input);
        String[] titleWords = parseTitle(input);

        HashMap<String, ArrayList<String>> mapCreation = createKWICMap(titleWords, stopWords);
        printMap(mapCreation);
    }
}
