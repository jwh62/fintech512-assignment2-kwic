package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class input {

    public static ArrayList<String> readInput() {
        ArrayList<String> titles = new ArrayList<String>();

        Scanner scan = new Scanner(System.in);
        try {
            while (scan.hasNextLine()){
                String line = scan.nextLine();
                if (line.equals("DONE")) {
                    scan.close();
                    break;
                }
                else{
                    System.out.println(line);
                    titles.add(line.toString());
                }
            }
        } finally {
            if (scan != null)
                scan.close();
        }
        return titles;
    }


    public static String[] parseStopWords(ArrayList<String> allWords) {
        ArrayList<String> stopWords = new ArrayList<>();
        for (int i = 0; i < allWords.size(); i++) {
            if (allWords.get(i).equals("::")){
                break;
            }
            else {
                stopWords.add(allWords.get(i));
            }
        }

        String[] stopWordsArr = new String[stopWords.size()];
        stopWordsArr = stopWords.toArray(stopWordsArr);

        return stopWordsArr;

    }

    public static String[] parseTitle(List<String> allWords) {
        ArrayList<String> titleWords = new ArrayList<>();
        boolean ready = false;

        for (int i = 0; i < allWords.size(); i++) {
            if (ready) {
                titleWords.add(allWords.get(i));
            }

            if (allWords.get(i).equals("::")){
                ready = true;
            }

        }

        String[] titleWordsArr = new String[titleWords.size()];
        titleWordsArr = titleWords.toArray(titleWordsArr);

        return titleWordsArr;
    }
}
