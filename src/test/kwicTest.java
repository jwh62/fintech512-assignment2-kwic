package test;

import static main.input.*;
import static main.kwic.*;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import main.kwic;

import org.junit.jupiter.api.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

class kwicTest {
    @Test
    @Disabled // can't write test for stdin but wanted to include to show steps because
    // this is what I started with
    void testInput() {
        String[] words = {"This", "is", "a", "test"};
        ArrayList<String> expected =  new ArrayList<String>();
        expected.addAll(Arrays.asList(words));

        ArrayList<String> actual = readInput();
        assertEquals(expected,actual);

    }

    @Test
    void testParseWords() {
        String[] words = {"This", "is", "::", "a", "test"};
        ArrayList<String> wordsAL = new ArrayList<>();
        wordsAL.addAll(Arrays.asList(words));

        String[] stopWords = Arrays.copyOfRange(words, 0, 2);
        String[] titles = Arrays.copyOfRange(words, 3, 5);

        String[] actualStopWords = parseStopWords(wordsAL);
        String[] actualTitleWords = parseTitle(wordsAL);

        assertArrayEquals(stopWords, actualStopWords);
        assertArrayEquals(titles, actualTitleWords);
    }

    @Test
    void testRemoveStopWordsFromTitle(){
        String[] stopwords = {"is", "of", "a", "as", "the"};
        String actual = kwic.removeStopWord("a portrait of the Artist as a young man", stopwords);

        assertEquals("portrait Artist young man", actual);
    }



    @Test
    void testKeywordtoUPPER(){
        String title = "Ascent of Man";
        String keyword = "Man";

        String actual = keywordToUPPER(title, keyword);

        assertEquals("Ascent of MAN", actual);
    }

    @Test
    void testMapCreation() {
        String[] stopwords= {"is", "the", "of", "and", "as", "a", "but"};
        String[] titles = {"Descent of Man", "The Ascent of Man", "The Old Man and The Sea",
                "A Portrait of The Artist As a Young Man", "A Man is a Man but Bubblesort IS A DOG"};
        HashMap<String, ArrayList<String>> mapCreation = createKWICMap(titles, stopwords);
        String actual = returnMap(mapCreation);

        String expected = "a portrait of the ARTIST as a young man\nthe ASCENT of man\na man is a man but BUBBLESORT is a dog\nDESCENT of man\na man is a man but bubblesort is a DOG\ndescent of MAN\nthe ascent of MAN\nthe old MAN and the sea\na portrait of the artist as a young MAN\na MAN is a man but bubblesort is a dog\na man is a MAN but bubblesort is a dog\nthe OLD man and the sea\na PORTRAIT of the artist as a young man\nthe old man and the SEA\na portrait of the artist as a YOUNG man\n";
        assertEquals(expected, actual);


    }
}
